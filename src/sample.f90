
include 'bbque_types.f90'

! back and forth C.
subroutine BBQonSetup(*)
  use RTLIB_ExitCode
	real alpha, beta
  print *, "FORTRAN: onSetup"
  return RTLIB_OK
end subroutine BBQonSetup

! Configuration
! Configure parallelism and/or input data related parameters      
subroutine BBQonConfigure(*)
    use RTLIB_ExitCode
    use RTLIB_ResourceType
    integer proc_quota
    external BBQGetAssignedResources
    call BBQGetAssignedResources(PROC_ELEMENT, proc_quota)
    print *, "FORTRAN: onConfigure: CPU quota=", proc_quota
    return RTLIB_OK
end subroutine BBQonConfigure

! Run
! start the execution; computational code should be here      
subroutine BBQonRun(*) 
    use RTLIB_ExitCode
    integer BBQCycles
    external BBQCycles
    print *, "FORTRAN: onRun: Hello cycle=", BBQCycles()
    ! Return after 5 cycles
    if (BBQCycles() >= 5) then
        return RTLIB_EXC_WORKLOAD_NONE
    end if
    return RTLIB_OK
end subroutine BBQonRun
      
! Monitor
! Get information needed to decide whether to reallocate resources      
subroutine BBQonMonitor(*)
    use RTLIB_ExitCode
    external BBQGetCPS
    real cps
    cps = BBQGetCPS()
    print *, "FORTRAN: onMonitor: CPS=", cps
    return RTLIB_OK
end subroutine BBQonMonitor

! Cleanup
! Deallocate as needed (probably nothing), also write out data, etc.      
subroutine BBQonRelease(*)
    use RTLIB_ExitCode
    print *, "FORTRAN: onRelease"
    return RTLIB_OK
end subroutine BBQonRelease



ifdef CONFIG_SAMPLES_FORTRAN_HELLOFORTRAN

# Targets provided by this project
.PHONY: samples_fortran_hellofortran clean_samples_fortran_hellofortran

# Add this to the "samples" targets
samples_fortran: samples_fortran_hellofortran
clean_samples_fortran: clean_samples_fortran_hellofortran

MODULE_SAMPLES_FORTRAN_HELLOFORTRAN=samples/fortran/HelloFortran

samples_fortran_hellofortran: external
	@echo
	@echo "==== Building HelloFortran ($(BUILD_TYPE)) ===="
	@echo " Using GCC    : $(CC)"
	@echo " Target flags : $(TARGET_FLAGS)"
	@echo " Sysroot      : $(PLATFORM_SYSROOT)"
	@echo " BOSP Options : $(CMAKE_COMMON_OPTIONS)"
	@[ -d $(MODULE_SAMPLES_FORTRAN_HELLOFORTRAN)/build/$(BUILD_TYPE) ] || \
		mkdir -p $(MODULE_SAMPLES_FORTRAN_HELLOFORTRAN)/build/$(BUILD_TYPE) || \
		exit 1
	@cd $(MODULE_SAMPLES_FORTRAN_HELLOFORTRAN)/build/$(BUILD_TYPE) && \
		CC=$(CC) CFLAGS="$(TARGET_FLAGS)" \
		CXX=$(CXX) CXXFLAGS="$(TARGET_FLAGS)" \
		cmake $(CMAKE_COMMON_OPTIONS) ../.. || \
		exit 1
	@cd $(MODULE_SAMPLES_FORTRAN_HELLOFORTRAN)/build/$(BUILD_TYPE) && \
		make -j$(CPUS) install || \
		exit 1

clean_samples_fortran_hellofortran:
	@echo
	@echo "==== Clean-up HelloFortran Application ===="
	@[ ! -f $(BUILD_DIR)/usr/bin/hellofortran ] || \
		rm -f $(BUILD_DIR)/etc/bbque/recipes/HelloFortran*; \
		rm -f $(BUILD_DIR)/usr/bin/hellofortran*
	@rm -rf $(MODULE_SAMPLES_FORTRAN_HELLOFORTRAN)/build
	@echo

else # CONFIG_SAMPLES_FORTRAN_HELLOFORTRAN

samples_fortran_hellofortran:
	$(warning HelloFortran module disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_SAMPLES_FORTRAN_HELLOFORTRAN

